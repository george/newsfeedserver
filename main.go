package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/fogleman/gg"
	"github.com/gofiber/fiber/v2"
	"github.com/tkanos/gonfig"
	"github.com/ungerik/go-rss"
	"io"
	"log"
	"net/http"
	"time"
)

type Configuration struct {
	RssFeed          string `json:"rss_feed"`
	PresetValueColor string `json:"preset_value_color"`
	CustomValueColor string `json:"custom_value_color"`
}

var httpClient = &http.Client{}
var conf *Configuration

func main() {
	conf = &Configuration{}
	err := gonfig.GetConf("config.json", conf)
	if err != nil {
		panic(err)
	}

	if conf.RssFeed == "" {
		panic("invalid rss feed set")
	}

	app := fiber.New()

	app.Get("/", getImages)

	log.Fatal(app.Listen(":9812"))
}

func getImages(c *fiber.Ctx) error {
	var resp []string
	items, err := getRss(conf.RssFeed)
	if err != nil {
		return err
	}

	for _, item := range items {
		t, err := item.PubDate.Format(time.RFC822)
		if err != nil {
			continue
		}
		resp = append(resp, generateRssImage(item.Title, item.Description, t))
	}

	return c.JSON(resp)
}

func getRss(feedLink string) ([]rss.Item, error) {
	r, err := httpClient.Get(feedLink)
	if err != nil {
		return nil, err
	}

	channel, err := rss.Regular(r)
	if err != nil {
		return nil, err
	}

	return channel.Item, nil
}

func generateRssImage(itemTitle, itemDescription, pubTime string) string {
	presetValueColor := conf.PresetValueColor
	customValueColor := conf.CustomValueColor

	img := gg.NewContext(1024, 312)

	// Draw on the entire image, making it all white instead of transparent.
	img.SetHexColor("#FFFFFF")
	img.DrawRectangle(0, 0, 1024, 312)
	img.Fill()

	// Loading IBM Plex Mono
	face, err := gg.LoadFontFace("static/fonts/IBMPlexMono-Regular.ttf", 16)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	img.SetFontFace(face)

	// The following piece of code defines what is drawn on the image.
	img.SetHexColor(presetValueColor)
	img.DrawString("Title:", 20, 50)

	img.SetHexColor(customValueColor)
	img.DrawString(itemTitle, 30, 70)

	img.SetHexColor(presetValueColor)
	img.DrawString("Description:", 20, 90)

	img.SetHexColor(customValueColor)
	img.DrawString(itemDescription, 30, 110)

	img.SetHexColor(presetValueColor)
	img.DrawString("Published at: ", 30, 300)
	patW, _ := img.MeasureString("Published at: ")

	img.SetHexColor(customValueColor)
	img.DrawString(pubTime, 30+patW, 300)

	rssLogo, _ := gg.LoadImage("static/img/rss.png")
	img.DrawImage(rssLogo, 900, 210)
	var buf bytes.Buffer
	err = img.EncodePNG(&buf)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	bb, err := io.ReadAll(&buf)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	b64 := base64.StdEncoding.EncodeToString(bb)
	return b64
}
