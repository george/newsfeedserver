module newsFeedServer

go 1.17

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gofiber/fiber/v2 v2.30.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/paulrosania/go-charset v0.0.0-20190326053356-55c9d7a5834c // indirect
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f // indirect
	github.com/ungerik/go-rss v0.0.0-20200405130036-81ac15598626 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.34.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/image v0.0.0-20220321031419-a8550c1d254a // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
